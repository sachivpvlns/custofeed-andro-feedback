package com.custofeed.app.feedback.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.custofeed.app.feedback.R;
import com.custofeed.app.feedback.activity.FeedbackActivity;
import com.custofeed.app.feedback.adapter.CustomViewPager;
import com.custofeed.app.feedback.model.Answer;
import com.custofeed.app.feedback.model.Session;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;

public class FragmentChild extends Fragment {
    int pk;
    int position;
    String title, type;
    TextView textViewChildName;
    ArrayList<Integer> option_pks;
    ArrayList<String> option_titles;

    TextInputLayout name, mobile, email;
    MaterialBetterSpinner date, month, year, date_a, month_a, year_a;
    Button submit;
    String[] date_st = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14",
            "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
            "29", "30", "31"};
    String[] month_st = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};

    String[] year_st = {"1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978",
            "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989",
            "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999",
            "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        View view;
        if(bundle.getBoolean("START")){
            view = inflater.inflate(R.layout.fragment_start, container, false);
            Button buttonStart = (Button) view.findViewById(R.id.buttonStart);
            buttonStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    nextPage();
                }
            });
        }else if(bundle.getBoolean("NPS")){
            view = inflater.inflate(R.layout.fragment_nps, container, false);
            final LikeButton[] like_buttons = {
                    (LikeButton) view.findViewById(R.id.like1),
                    (LikeButton) view.findViewById(R.id.like2),
                    (LikeButton) view.findViewById(R.id.like3),
                    (LikeButton) view.findViewById(R.id.like4),
                    (LikeButton) view.findViewById(R.id.like5),
                    (LikeButton) view.findViewById(R.id.like6),
                    (LikeButton) view.findViewById(R.id.like7),
                    (LikeButton) view.findViewById(R.id.like8),
                    (LikeButton) view.findViewById(R.id.like9),
                    (LikeButton) view.findViewById(R.id.like10),
            };

            for(int i=0;i<like_buttons.length;i++){
                final int val = i+1;
                like_buttons[i].setOnLikeListener(new OnLikeListener() {
                    @Override
                    public void liked(LikeButton likeButton) {
                        for(int j=0;j<10;j++) {
                            like_buttons[j].setLiked(false);
                        }
                        for(int j=0;j<val;j++) {
                            like_buttons[j].setLiked(true);
                        }

                        //Store answer in the session
                        Session session = (Session) getActivity().getApplicationContext();
                        session.feedback.setNPS(val);

                        nextPage();
                    }

                    @Override
                    public void unLiked(LikeButton likeButton) {
                        for(int j=0;j<10;j++) {
                            like_buttons[j].setLiked(false);
                        }
                        for(int j=0;j<val;j++) {
                            like_buttons[j].setLiked(true);
                        }

                        //Store answer in the session
                        Session session = (Session) getActivity().getApplicationContext();
                        session.feedback.setNPS(val);

                        nextPage();
                    }
                });
            }
        }else if(bundle.getBoolean("COMMENT")){
            view = inflater.inflate(R.layout.fragment_comment, container, false);
            Button buttonNext = (Button) view.findViewById(R.id.buttonNext);
            final TextView comment = (TextView) view.findViewById(R.id.comment);
            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //Store answer in the session
                    Session session = (Session) getActivity().getApplicationContext();
                    session.feedback.setComment(comment.getText().toString());

                    nextPage();
                }
            });
        }else if(bundle.getBoolean("DETAILS")){
            view = inflater.inflate(R.layout.fragment_details, container, false);

            name = (TextInputLayout) view.findViewById(R.id.name_details);
            mobile = (TextInputLayout) view.findViewById(R.id.mobile_details);
            email = (TextInputLayout) view.findViewById(R.id.email_detail);
            date = (MaterialBetterSpinner) view.findViewById(R.id.date);
            month = (MaterialBetterSpinner) view.findViewById(R.id.month);
            year = (MaterialBetterSpinner) view.findViewById(R.id.year);
            date_a = (MaterialBetterSpinner) view.findViewById(R.id.date_a);
            month_a = (MaterialBetterSpinner) view.findViewById(R.id.month_a);
            year_a = (MaterialBetterSpinner) view.findViewById(R.id.year_a);
            ArrayAdapter<String> adapter_date = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.simple_dropdown_item_1line, date_st);
            date.setAdapter(adapter_date);
            ArrayAdapter<String> adapter_month = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.simple_dropdown_item_1line, month_st);
            month.setAdapter(adapter_month);
            ArrayAdapter<String> adapter_year = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.simple_dropdown_item_1line, year_st);
            year.setAdapter(adapter_year);

            ArrayAdapter<String> adapter_dates = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.simple_dropdown_item_1line, date_st);
            date_a.setAdapter(adapter_dates);
            ArrayAdapter<String> adapter_months = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.simple_dropdown_item_1line, month_st);
            month_a.setAdapter(adapter_months);
            ArrayAdapter<String> adapter_years = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.simple_dropdown_item_1line, year_st);
            year_a.setAdapter(adapter_years);

            Button buttonSubmit = (Button) view.findViewById(R.id.submit_detail);
            buttonSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String name_st = name.getEditText().getText().toString();
                    String mobile_st = mobile.getEditText().getText().toString();
                    String email_st = email.getEditText().getText().toString();
                    Session session = (Session) getActivity().getApplicationContext();

                    String dob = year.getText() + "-" + month.getText() + "-" + date.getText();
                    if(dob.equals("--")){
                        dob = "";
                    }
                    String anni = year_a.getText() + "-" + month_a.getText() + "-" + date_a.getText();
                    if(anni.equals("--")){
                        anni = "";
                    }

                    if (name_st.isEmpty()) {
                        name.setErrorEnabled(true);
                        name.setError("This field can't be empty");
                        return;
                    }
                    name.setError("");

                    if (mobile_st.isEmpty()) {
                        mobile.setErrorEnabled(true);
                        mobile.setError("This field can't empty");
                        return;
                    }else if(!mobile_st.matches("^[789]\\d{9}$")){
                        mobile.setErrorEnabled(true);
                        mobile.setError("Invalid mobile number");
                        return;
                    }
                    mobile.setError("");

                    session.customer.setName(name_st);
                    session.customer.setMobile(mobile_st);
                    session.customer.setEmail(email_st);
                    session.customer.setDateBDay(dob);
                    session.customer.setDateAnni(anni);

                    ((FeedbackActivity) getActivity()).nextActivity();
                }
            });
        }else{
            view = inflater.inflate(R.layout.fragment_child, container, false);
            pk = bundle.getInt("pk");
            position = bundle.getInt("position");
            title = bundle.getString("title");
            type = bundle.getString("type");

            option_titles = new ArrayList<String>();
            option_titles = bundle.getStringArrayList("options");

            option_pks = new ArrayList<Integer>();
            option_pks = bundle.getIntegerArrayList("option_pks");

            getIDs(view);
        }
        return view;
    }

    private void getIDs(View view) {
        textViewChildName = (TextView) view.findViewById(R.id.textViewChild);
        textViewChildName.setText(title);
        LinearLayout optionsLayout = (LinearLayout) view.findViewById(R.id.optionsLayout);
        LinearLayout ratingBar = (LinearLayout) view.findViewById(R.id.ratingBar);

        if(type != null){
            switch(type){
                case "RATING":
                    optionsLayout.setVisibility(View.GONE);
                    final LikeButton[] like_buttons = {
                            (LikeButton) view.findViewById(R.id.like1),
                            (LikeButton) view.findViewById(R.id.like2),
                            (LikeButton) view.findViewById(R.id.like3),
                            (LikeButton) view.findViewById(R.id.like4),
                            (LikeButton) view.findViewById(R.id.like5)
                    };

                    for(int i=0;i<like_buttons.length;i++){
                        final int val = i+1;
                        like_buttons[i].setOnLikeListener(new OnLikeListener() {
                            @Override
                            public void liked(LikeButton likeButton) {
                                for(int j=0;j<5;j++) {
                                    like_buttons[j].setLiked(false);
                                }
                                for(int j=0;j<val;j++) {
                                    like_buttons[j].setLiked(true);
                                }

                                //Store answer in the session
                                Session session = (Session) getActivity().getApplicationContext();
                                Answer answer;
                                if (session.checkAnswer(position)){
                                    answer = session.getAnswer(position);
                                }else{
                                    answer = new Answer();
                                }
                                answer.setQuestion(pk);
                                answer.setText(Integer.toString(val));
                                session.setAnswer(answer);
                                nextPage();
                            }

                            @Override
                            public void unLiked(LikeButton likeButton) {
                                for(int j=0;j<5;j++) {
                                    like_buttons[j].setLiked(false);
                                }
                                for(int j=0;j<val;j++) {
                                    like_buttons[j].setLiked(true);
                                }

                                //Store answer in the session
                                Session session = (Session) getActivity().getApplicationContext();
                                Answer answer;
                                if (session.checkAnswer(position)){
                                    answer = session.getAnswer(position);
                                }else{
                                    answer = new Answer();
                                }
                                answer.setQuestion(pk);
                                answer.setText(Integer.toString(val));
                                session.setAnswer(answer);

                                nextPage();
                            }
                        });
                    }
                    break;
                case "MCQ":
                    ratingBar.setVisibility(View.GONE);
                    final RadioButton[] rb = new RadioButton[option_titles.size()];
                    RadioGroup rg = new RadioGroup(getActivity());
                    rg.setOrientation(RadioGroup.VERTICAL);
                    for(int i = 0; i < option_titles.size(); i++){
                        final int val = i;
                        rb[i]  = new RadioButton(getActivity());
                        rb[i].setText(option_titles.get(i));
                        rb[i].setTextColor(getResources().getColor(R.color.white));
                        rb[i].setId(i + 100);
                        rb[i].setTextSize(20);

                        rb[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                //Store answer in the session
                                Session session = (Session) getActivity().getApplicationContext();
                                Answer answer;
                                if (session.checkAnswer(position)){
                                    answer = session.getAnswer(position);
                                }else{
                                    answer = new Answer();
                                }
                                answer.setQuestion(pk);
                                answer.setOption(option_pks.get(val));
                                answer.setText(option_titles.get(val));
                                session.setAnswer(answer);

                                nextPage();
                            }
                        });

                        rg.addView(rb[i]);
                    }
                    optionsLayout.addView(rg);//you add the whole RadioGroup to the layout
                    break;
            }
        }
    }

    public void nextPage(){
        final CustomViewPager viewPager = ((FeedbackActivity) getActivity()).fragmentParent.viewPager;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            }
        }, 800);
    }
}