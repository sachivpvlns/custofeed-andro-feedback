package com.custofeed.app.feedback.model;

/**
 * Created by sachiv on 3/31/2016.
 */
public class Feedback {
    //private variables
    private int ID;
    private int pk;
    private String pin;
    private int nps;
    private String comment;
    private int customer;
    private String created_at;
    private int sync =0;

    // Empty constructor
    public Feedback(){}

    // constructor
    public Feedback(int pk, String pin, int nps, String comment, int customer){
        this.pk = pk;
        this.pin = pin;
        this.nps = nps;
        this.comment = comment;
        this.customer = customer;
        this.sync = 0;
    }

    public int getID(){
        return this.ID;
    }
    public void setID(int ID){
        this.ID = ID;
    }

    public int getPk(){
        return this.pk;
    }
    public void setPk(int pk){
        this.pk = pk;
    }

    public String getPin(){
        return this.pin;
    }
    public void setPin(String pin){
        this.pin = pin;
    }

    public int getNPS(){
        return this.nps;
    }
    public void setNPS(int nps){
        this.nps = nps;
    }

    public String getComment(){
        return this.comment;
    }
    public void setComment(String comment){
        this.comment = comment;
    }

    public int getCustomer(){
        return this.customer;
    }
    public void setCustomer(int customer){
        this.customer = customer;
    }

    public String getCreatedAt(){
        return this.created_at;
    }
    public void setCreatedAt(String created_at){
        this.created_at = created_at;
    }

    public int getSync(){
        return this.sync;
    }
    public void setSync(int sync){
        this.sync = sync;
    }
}
