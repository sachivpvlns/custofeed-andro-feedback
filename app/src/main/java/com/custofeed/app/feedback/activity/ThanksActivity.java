package com.custofeed.app.feedback.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.custofeed.app.feedback.R;
import com.custofeed.app.feedback.model.Answer;
import com.custofeed.app.feedback.model.DatabaseHandler;
import com.custofeed.app.feedback.model.Option;
import com.custofeed.app.feedback.model.Session;
import com.custofeed.app.feedback.utils.SharedPreference;

import java.util.List;

public class ThanksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);

        Session session = (Session) getApplicationContext();

        DatabaseHandler mydb = new DatabaseHandler(this);

        //Add a customer to DB  -------------
        int customer_id = (int) mydb.addCustomer(session.customer); //Insert customer into DB

        //Add a feedback to DB  -------------
        session.feedback.setCustomer(customer_id);
        int feedback_id = (int) mydb.addFeedback(session.feedback); //Insert customer into DB

        //Add all answers to DB  -------------
        List<Answer> answers = session.getAnswers();
        for (int i=0;i<answers.size();i++){
            Option option = mydb.getOptionFromTitle(answers.get(i).getQuestion(), answers.get(i).getText());
            answers.get(i).setOption(option.getPK());
            answers.get(i).setFeedback(feedback_id);
            mydb.addAnswer(answers.get(i));
        }

        startForceSync();
        startScheduleSync();
        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    nextActivity();
                }
            }
        };
        timerThread.start();
    }

    public void nextActivity(){
        finish();
        Intent myIntent = new Intent(this,PinActivity.class);
        this.startActivity(myIntent);
    }

    private void startForceSync() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        Account account = new Account(getCurrentAccount(), SharedPreference.PACKAGE_SYNC);
        ContentResolver.requestSync(account, SharedPreference.PACKAGE_NAME, bundle);

        ContentResolver.setIsSyncable(account, SharedPreference.PACKAGE_NAME, 1);
        ContentResolver.setSyncAutomatically(account, SharedPreference.PACKAGE_NAME, true);
    }

    private void startScheduleSync() {
        Bundle bundle = new Bundle();
        Account account = new Account(getCurrentAccount(), SharedPreference.PACKAGE_SYNC);
        ContentResolver.addPeriodicSync(account, SharedPreference.PACKAGE_NAME, bundle, 15);
    }

    private String getCurrentAccount() {
        Account[] accounts = AccountManager.get(this).getAccountsByType(SharedPreference.PACKAGE_SYNC);
        return accounts.length < 0 ? "" : accounts[0].name;
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
