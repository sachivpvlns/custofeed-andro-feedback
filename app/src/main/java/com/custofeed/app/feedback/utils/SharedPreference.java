package com.custofeed.app.feedback.utils;

/**
 * Created by sachiv on 20-Aug-16.
 */
public class SharedPreference {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public SharedPreference() {}

    public static final String PACKAGE_NAME = "com.custofeed.app.feedback";
    public static final String PACKAGE_SYNC = PACKAGE_NAME + ".sync";

    public class Account {
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";

        public static final String IS_LOGGED = "isLogged";
        public static final String SESSION_EXPIRE = "loginExpire";

        public static final String AUTH_TOKEN = "auth_token";
    }

    public class Store {
        public static final String NAME = "store_name";
    }

    public class Company {
        public static final String NAME = "company_name";
        public static final String SECTOR = "company_sector";
    }

    public class Feedback {
        public static final String PK = "feedback_pk";
        public static final String TITLE = "feedback_title";
    }
}
