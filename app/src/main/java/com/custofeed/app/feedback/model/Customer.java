package com.custofeed.app.feedback.model;

/**
 * Created by sachiv on 3/31/2016.
 */
public class Customer {
    //private variables
    private String name;
    private String mobile;
    private String email;
    private String date_bday;
    private String date_anni;

    // Empty constructor
    public Customer(){}

    // constructor
    public Customer(String name, String mobile, String email, String date_bday,
                    String date_anni){
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.date_bday = date_bday;
        this.date_anni = date_anni;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getMobile(){
        return this.mobile;
    }
    public void setMobile(String mobile){
        this.mobile = mobile;
    }

    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }

    public String getDateBDay(){
        if (this.date_bday != null)
            return this.date_bday;
        else
            return "null";
    }
    public void setDateBDay(String date_bday){
        this.date_bday = date_bday;
    }

    public String getDateAnni(){
        if (this.date_anni != null)
            return this.date_anni;
        else
            return "null";
    }
    public void setDateAnni(String date_anni){
        this.date_anni = date_anni;
    }
}
