package com.custofeed.app.feedback.model;

/**
 * Created by sachiv on 4/1/2016. old
 */
public class Option {
    //private variables
    private int id;
    private int question;
    private int pk=0;
    private String title;

    // Empty constructor
    public Option() {
    }

    // constructor
    public Option(int question, int pk, String title) {
        this.question = question;
        this.pk = pk;
        this.title = title;
    }

    public int getQuestion() {
        return this.question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

    public int getPK() {
        return this.pk;
    }

    public void setPK(int pk) {
        this.pk = pk;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
