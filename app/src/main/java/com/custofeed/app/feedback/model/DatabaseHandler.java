package com.custofeed.app.feedback.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "CustoFeed";

    // Feedbacks table name
    private static final String TABLE_FEEDBACKS = "feedbacks";
    // Customers table name
    private static final String TABLE_CUSTOMERS = "customers";
    // Employees table name
    private static final String TABLE_EMPLOYEES = "employees";
    // Questions table name
    private static final String TABLE_QUESTIONS = "questions";
    // Options table name
    private static final String TABLE_OPTIONS = "options";
    // Answers table name
    private static final String TABLE_ANSWERS = "answers";

    // Feedbacks Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_PIN = "pin";
    private static final String KEY_COMMENT = "comment";
    private static final String KEY_NAME = "name";
    private static final String KEY_MOBILE = "mobile";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_DATE_BDAY = "date_bday";
    private static final String KEY_DATE_ANNI = "date_anni";
    private static final String KEY_NPS = "nps";
    private static final String KEY_CUSTOMER = "customer";

    private static final String KEY_CREATED_AT = "created_at";
    private static final String KEY_SYNC = "sync";

    private static final String KEY_PK = "pk";
    private static final String KEY_TITLE = "title";
    private static final String KEY_TYPE = "type";

    private static final String KEY_QUESTION = "question";
    private static final String KEY_OPTION = "option";
    private static final String KEY_FEEDBACK = "feedback";
    private static final String KEY_TEXT = "text";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CUSTOMER_TABLE = "CREATE TABLE " + TABLE_CUSTOMERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NAME + " TEXT,"
                + KEY_MOBILE + " TEXT,"
                + KEY_EMAIL + " TEXT,"
                + KEY_DATE_BDAY + " DATE,"
                + KEY_DATE_ANNI + " DATE"
                + ")";
        db.execSQL(CREATE_CUSTOMER_TABLE);

        String CREATE_FEEDBACKS_TABLE = "CREATE TABLE " + TABLE_FEEDBACKS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_PK + " INTEGER,"
                + KEY_PIN + " TEXT,"
                + KEY_NPS + " INTEGER,"
                + KEY_COMMENT + " TEXT,"
                + KEY_CUSTOMER + " INTEGER,"
                + KEY_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                + KEY_SYNC + " INTEGER"
                + ")";
        db.execSQL(CREATE_FEEDBACKS_TABLE);

        String CREATE_PINS_TABLE = "CREATE TABLE " + TABLE_EMPLOYEES + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_PIN + " TEXT UNIQUE"
                + ")";
        db.execSQL(CREATE_PINS_TABLE);

        String CREATE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_PK + " INTEGER,"
                + KEY_TYPE + " TEXT,"
                + KEY_TITLE + " TEXT"
                + ")";
        db.execSQL(CREATE_QUESTIONS_TABLE);

        String CREATE_OPTIONS_TABLE = "CREATE TABLE " + TABLE_OPTIONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_QUESTION + " INTEGER,"
                + KEY_PK + " INTEGER,"
                + KEY_TITLE + " TEXT"
                + ")";
        db.execSQL(CREATE_OPTIONS_TABLE);

        String CREATE_ANSWERS_TABLE = "CREATE TABLE " + TABLE_ANSWERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_FEEDBACK + " INTEGER,"
                + KEY_QUESTION + " INTEGER,"
                + KEY_OPTION + " INTEGER,"
                + KEY_TEXT + " TEXT"
                + ")";
        db.execSQL(CREATE_ANSWERS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < newVersion){
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMERS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_FEEDBACKS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMPLOYEES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);

            // Create tables again
            onCreate(db);
        }
    }

    public String[] getColumnNames(String table) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor dbCursor = db.query(table, null, null, null, null, null, null);
        String[] columnNames = dbCursor.getColumnNames();

        dbCursor.close();
        db.close();
        return columnNames;
    }

    /*
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    /* ////////////////////////////////////////////////////////////////////
    CUSTOMERS TABLE
    /////////////////////////////////////////////////////////////////////// */

    // Adding new Customer
    public long addCustomer(Customer customer) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, customer.getName());
        values.put(KEY_MOBILE, customer.getMobile());
        values.put(KEY_EMAIL, customer.getEmail());
        values.put(KEY_DATE_BDAY, customer.getDateBDay());
        values.put(KEY_DATE_ANNI, customer.getDateAnni());

        // Inserting Row
        long id = db.insert(TABLE_CUSTOMERS, null, values);
        db.close(); // Closing database connection

        return id;
    }

    // Getting single Customer
    public Customer getCustomer(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CUSTOMERS, new String[]{KEY_ID,
                        KEY_NAME,
                        KEY_MOBILE,
                        KEY_EMAIL,
                        KEY_DATE_BDAY,
                        KEY_DATE_ANNI}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Customer customer = new Customer(cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4), cursor.getString(5));

        db.close(); // Closing database connection
        // return customer
        return customer;
    }

    // Getting All Customers
    public List<Customer> getAllCustomers() {
        List<Customer> list = new ArrayList<Customer>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CUSTOMERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Customer customer = new Customer();
                customer.setName(cursor.getString(1));
                customer.setMobile(cursor.getString(2));
                customer.setEmail(cursor.getString(3));
                customer.setDateBDay(cursor.getString(4));
                customer.setDateAnni(cursor.getString(5));

                // Adding feedback to list
                list.add(customer);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection

        // return feedback list
        return list;
    }


    // Getting Customers Count
    public int getCustomersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CUSTOMERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    /* ////////////////////////////////////////////////////////////////////
    FEEDBACKS TABLE
    /////////////////////////////////////////////////////////////////////// */

    // Adding new feedback
    public long addFeedback(Feedback feedback) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PK, feedback.getPk());
        values.put(KEY_PIN, feedback.getPin());
        values.put(KEY_NPS, feedback.getNPS());
        values.put(KEY_COMMENT, feedback.getComment());
        values.put(KEY_CUSTOMER, feedback.getCustomer());
        values.put(KEY_SYNC, 0);

        // Inserting Row
        long id = db.insert(TABLE_FEEDBACKS, null, values);
        db.close(); // Closing database connection

        return id;
    }

    // Getting single feedback
    public Feedback getFeedback(int pk) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_FEEDBACKS, new String[]{KEY_ID,
                        KEY_PK,
                        KEY_PIN,
                        KEY_NPS,
                        KEY_COMMENT,
                        KEY_CUSTOMER,
                        KEY_CREATED_AT,
                        KEY_SYNC}, KEY_PK + "=?",
                new String[]{String.valueOf(pk)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        if(cursor != null && cursor.moveToFirst()){
            Feedback feedback = new Feedback(cursor.getInt(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getInt(5));
            feedback.setID(cursor.getInt(0));
            feedback.setCreatedAt(cursor.getString(6));
            feedback.setSync(cursor.getInt(7));
            cursor.close();
            return feedback;
        }else{
            return new Feedback();
        }
    }

    // Getting single feedback
    public Feedback getFeedbackByTimestamp(String timestamp) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_FEEDBACKS, new String[]{KEY_ID,
                        KEY_PK,
                        KEY_PIN,
                        KEY_NPS,
                        KEY_COMMENT,
                        KEY_CUSTOMER,
                        KEY_CREATED_AT,
                        KEY_SYNC}, KEY_CREATED_AT + "=?",
                new String[]{timestamp}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        if(cursor != null && cursor.moveToFirst()){
            Feedback feedback = new Feedback(cursor.getInt(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getInt(5));
            feedback.setID(cursor.getInt(0));
            feedback.setCreatedAt(cursor.getString(6));
            feedback.setSync(cursor.getInt(7));
            cursor.close();
            return feedback;
        }else{
            return new Feedback();
        }
    }

    // Getting All Feedbacks
    public List<Feedback> getAllFeedbacks() {
        List<Feedback> feedbackList = new ArrayList<Feedback>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FEEDBACKS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Feedback feedback = new Feedback(cursor.getInt(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getInt(5));
                feedback.setID(cursor.getInt(0));
                feedback.setCreatedAt(cursor.getString(6));
                feedback.setSync(cursor.getInt(7));

                // Adding feedback to list
                feedbackList.add(feedback);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection

        // return feedback list
        return feedbackList;
    }

    // Getting Unsynced Feedbacks
    public List<Feedback> getUnsyncedFeedbacks() {
        List<Feedback> feedbackList = new ArrayList<Feedback>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_FEEDBACKS + " WHERE sync='0'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Feedback feedback = new Feedback(cursor.getInt(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getInt(5));
                feedback.setID(cursor.getInt(0));
                feedback.setCreatedAt(cursor.getString(6));
                feedback.setSync(cursor.getInt(7));

                // Adding feedback to list
                feedbackList.add(feedback);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection

        // return feedback list
        return feedbackList;
    }

    // Updating single feedback
    public int updateFeedback(Feedback feedback) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PK, feedback.getPk());
        values.put(KEY_PIN, feedback.getPin());
        values.put(KEY_NPS, feedback.getNPS());
        values.put(KEY_COMMENT, feedback.getComment());
        values.put(KEY_CUSTOMER, feedback.getCustomer());
        values.put(KEY_CREATED_AT, feedback.getCreatedAt());
        values.put(KEY_SYNC, feedback.getSync());

        int ret = db.update(TABLE_FEEDBACKS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(feedback.getID())});

        db.close();

        // updating row
        return ret;
    }

    // Getting feedbacks Count
    public int getFeedbacksCount() {
        String countQuery = "SELECT  * FROM " + TABLE_FEEDBACKS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    /* ////////////////////////////////////////////////////////////////////
    EMPLOYEES TABLE
    /////////////////////////////////////////////////////////////////////// */

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new employee
    public long addEmployee(Employee employee) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PIN, employee.getPin());

        // Inserting Row
        long id = db.insert(TABLE_EMPLOYEES, null, values);
        db.close(); // Closing database connection

        return id;
    }

    // Getting single employee
    Employee getEmployee(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EMPLOYEES, new String[]{KEY_ID, KEY_PIN}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Employee employee = new Employee(cursor.getString(1));

        cursor.close();
        db.close(); // Closing database connection
        // return employee
        return employee;
    }

    // Getting All Employees
    public List<Employee> getAllEmployees() {
        List<Employee> employeeList = new ArrayList<Employee>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EMPLOYEES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Employee employee = new Employee();
                employee.setPin(cursor.getString(1));

                // Adding employee to list
                employeeList.add(employee);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close(); // Closing database connection

        // return employee list
        return employeeList;
    }

    // Deleting single employee
    public void deleteAllEmployees() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EMPLOYEES, null, null);
        db.close();
    }


    // Getting employees Count
    public int getEmployeesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_EMPLOYEES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


    /* ////////////////////////////////////////////////////////////////////
    QUESTIONS TABLE
    /////////////////////////////////////////////////////////////////////// */

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new option
    public long addQuestion(Question question) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PK, question.getPK());
        values.put(KEY_TYPE, question.getType());
        values.put(KEY_TITLE, question.getTitle());
        // Inserting Row
        long id = db.insert(TABLE_QUESTIONS, null, values);
        db.close(); // Closing database connection

        return id;
    }

    // Getting single option
    public Question getQuestion(int pk) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_QUESTIONS, new String[]{KEY_PK, KEY_TYPE, KEY_TITLE}, KEY_PK + "=?",
                new String[]{String.valueOf(pk)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Question question = new Question(cursor.getInt(0), cursor.getString(1), cursor.getString(2));

        cursor.close();
        db.close(); // Closing database connection
        // return employee
        return question;
    }

    // Getting All options
    public List<Question> getAllQuestions() {
        List<Question> list = new ArrayList<Question>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Question question = new Question();
                question.setPK(Integer.parseInt(cursor.getString(1)));
                question.setType(cursor.getString(2));
                question.setTitle(cursor.getString(3));

                // Adding employee to list
                list.add(question);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close(); // Closing database connection

        // return employee list
        return list;
    }

    // Updating single option
    public int updateQuestion(Question question) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TYPE, question.getType());
        values.put(KEY_TITLE, question.getTitle());

        db.close(); // Closing database connection

        // updating row
        return db.update(TABLE_QUESTIONS, values, KEY_PK + " = ?",
                new String[] { String.valueOf(question.getPK())});
    }

    // Deleting single option
    public void deleteQuestion(Question question) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_QUESTIONS, KEY_PK + " = ?",
                new String[]{String.valueOf(question.getPK())});
        db.close();
    }

    // Deleting single option
    public void deleteAllQuestions() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_QUESTIONS, null, null);
        db.close();
    }


    // Getting options Count
    public int getQuestionsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_QUESTIONS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


    /* ////////////////////////////////////////////////////////////////////
    OPTIONS TABLE
    /////////////////////////////////////////////////////////////////////// */

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new option
    public long addOption(Option option) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_QUESTION, option.getQuestion());
        values.put(KEY_PK, option.getPK());
        values.put(KEY_TITLE, option.getTitle());

        // Inserting Row
        long id =db.insert(TABLE_OPTIONS, null, values);
        db.close(); // Closing database connection

        return id;
    }

    // Getting single option
    Option getOption(int pk) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_OPTIONS, new String[]{KEY_QUESTION, KEY_PK, KEY_TITLE}, KEY_PK + "=?",
                new String[]{String.valueOf(pk)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Option option = new Option(cursor.getInt(0), cursor.getInt(1), cursor.getString(2));

        cursor.close();
        db.close(); // Closing database connection
        // return employee
        return option;
    }

    // Getting All options
    public List<Option> getAllOptions() {
        List<Option> list = new ArrayList<Option>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_OPTIONS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Option option = new Option();
                option.setQuestion(Integer.parseInt(cursor.getString(1)));
                option.setPK(Integer.parseInt(cursor.getString(2)));
                option.setTitle(cursor.getString(3));

                // Adding employee to list
                list.add(option);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close(); // Closing database connection

        // return employee list
        return list;
    }

    // Getting options of a given question
    public List<Option> getQuestionOptions(int question) {
        List<Option> list = new ArrayList<Option>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_OPTIONS + " WHERE " + KEY_QUESTION + " = " + question;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Option option = new Option();
                option.setQuestion(Integer.parseInt(cursor.getString(1)));
                option.setPK(Integer.parseInt(cursor.getString(2)));
                option.setTitle(cursor.getString(3));

                // Adding employee to list
                list.add(option);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close(); // Closing database connection

        // return employee list
        return list;
    }

    // Updating single option
    public int updateOption(Option option) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_QUESTION, option.getQuestion());
        values.put(KEY_TITLE, option.getTitle());

        db.close(); // Closing database connection

        // updating row
        return db.update(TABLE_OPTIONS, values, KEY_PK + " = ?",
                new String[] { String.valueOf(option.getPK())});
    }

    // Deleting single option
    public void deleteOption(Option option) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_OPTIONS, KEY_PK + " = ?",
                new String[]{String.valueOf(option.getPK())});
        db.close();
    }

    // Deleting single option
    public void deleteAllOptions() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_OPTIONS, null, null);
        db.close();
    }


    // Getting options Count
    public int getOptionsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_OPTIONS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // Getting single option from title and question id
    // Getting Answers of a Feedback
    public Option getOptionFromTitle(int question_pk, String title) {
        Option option = new Option();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_OPTIONS + " WHERE "+KEY_QUESTION+"='"+question_pk+"' AND "+KEY_TITLE+"='"+title+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                option.setQuestion(Integer.parseInt(cursor.getString(1)));
                option.setPK(Integer.parseInt(cursor.getString(2)));
                option.setTitle(cursor.getString(3));
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection

        // return feedback list
        return option;
    }

    /* ////////////////////////////////////////////////////////////////////
    ANSWERS TABLE
    /////////////////////////////////////////////////////////////////////// */

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new option
    public long addAnswer(Answer answer) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FEEDBACK, answer.getFeedback());
        values.put(KEY_QUESTION, answer.getQuestion());
        values.put(KEY_OPTION, answer.getOption());
        values.put(KEY_TEXT, answer.getText());

        // Inserting Row
        long id = db.insert(TABLE_ANSWERS, null, values);
        db.close(); // Closing database connection

        return id;
    }

    // Getting single option
    Answer getAnswer(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ANSWERS, new String[]{KEY_FEEDBACK, KEY_QUESTION, KEY_OPTION, KEY_TEXT}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Answer answer = new Answer(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3));

        cursor.close();
        db.close(); // Closing database connection
        // return employee
        return answer;
    }

    // Getting All options
    public List<Answer> getAllAnswers() {
        List<Answer> list = new ArrayList<Answer>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ANSWERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Answer answer = new Answer();
                answer.setFeedback(Integer.parseInt(cursor.getString(1)));
                answer.setQuestion(Integer.parseInt(cursor.getString(2)));
                answer.setOption(Integer.parseInt(cursor.getString(3)));
                answer.setText(cursor.getString(4));

                // Adding employee to list
                list.add(answer);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close(); // Closing database connection

        // return employee list
        return list;
    }

    // Getting Answers of a Feedback
    public List<Answer> getFeedbackAnswers(int feedback_pk) {
        List<Answer> list = new ArrayList<Answer>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ANSWERS + " WHERE "+KEY_FEEDBACK+"='"+feedback_pk+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Answer answer = new Answer();
                answer.setFeedback(Integer.parseInt(cursor.getString(1)));
                answer.setQuestion(Integer.parseInt(cursor.getString(2)));
                answer.setOption(Integer.parseInt(cursor.getString(3)));
                answer.setText(cursor.getString(4));

                // Adding employee to list
                list.add(answer);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection

        // return feedback list
        return list;
    }

    // Deleting single option
    public void deleteAllAnswers() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ANSWERS, null, null);
        db.close();
    }


    // Getting employees Count
    public int getAnswersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ANSWERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

}
