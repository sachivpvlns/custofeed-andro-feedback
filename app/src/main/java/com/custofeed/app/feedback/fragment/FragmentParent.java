package com.custofeed.app.feedback.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import com.custofeed.app.feedback.R;
import com.custofeed.app.feedback.adapter.CustomScroller;
import com.custofeed.app.feedback.adapter.CustomViewPager;
import com.custofeed.app.feedback.adapter.ViewPagerAdapter;
import com.custofeed.app.feedback.model.DatabaseHandler;
import com.custofeed.app.feedback.model.Option;
import com.custofeed.app.feedback.model.Question;
import com.custofeed.app.feedback.utils.SwipeDirection;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class FragmentParent extends Fragment {
    public CustomViewPager viewPager;
    public ViewPagerAdapter adapter;
    private CustomScroller customScroller;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent, container, false);
        getIDs(view);

        return view;
    }

    private void getIDs(View view) {
        viewPager = (CustomViewPager) view.findViewById(R.id.my_viewpager);
        viewPager.setOffscreenPageLimit(100);
        viewPager.setAllowedSwipeDirection(SwipeDirection.left);
        adapter = new ViewPagerAdapter(getFragmentManager(), getActivity(), viewPager);
        viewPager.setAdapter(adapter);
    }

    public void addPage(Question question, int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("pk", question.getPK());
        bundle.putInt("position",position);
        bundle.putString("title", question.getTitle());
        bundle.putString("type", question.getType());

        if(question.getType().equals("MCQ")){
            DatabaseHandler mydb;
            mydb = new DatabaseHandler(getActivity());
            List<Option> options = mydb.getQuestionOptions(question.getPK());
            ArrayList<String> option_titles =new ArrayList<String>();
            for (int i = 0; i < options.size(); i++) {
                option_titles.add(options.get(i).getTitle());
            }
            bundle.putStringArrayList("options", option_titles);
            ArrayList<Integer> option_pks =new ArrayList<Integer>();
            for (int i = 0; i < options.size(); i++) {
                option_pks.add(options.get(i).getPK());
            }
            bundle.putIntegerArrayList("option_pks", option_pks);
        }

        FragmentChild fragmentChild = new FragmentChild();
        fragmentChild.setArguments(bundle);
        adapter.addFrag(fragmentChild, question.getTitle());
        adapter.notifyDataSetChanged();

        viewPager.setCurrentItem(0);
    }

    public void addSpecialPage(String name) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(name, true);
        FragmentChild fragmentChild = new FragmentChild();
        fragmentChild.setArguments(bundle);
        adapter.addFrag(fragmentChild, name);
        adapter.notifyDataSetChanged();

        viewPager.setCurrentItem(0);
    }

    private void setCustomScrollerToViewPager(Interpolator interpolator, int duration) {
        try {
            customScroller = new CustomScroller(getActivity(), interpolator, duration);
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager, customScroller);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
