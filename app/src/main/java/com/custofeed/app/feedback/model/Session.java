package com.custofeed.app.feedback.model;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sachiv on 21-Mar-16.
 */

public class Session extends Application {

    public Customer customer = new Customer();
    public Feedback feedback = new Feedback();

    private List<Answer> answers = new ArrayList<Answer>();

    public void setAnswer(Answer answer) {
        this.answers.add(answer);
    }

    public Answer getAnswer(int position) {
        return this.answers.get(position);
    }

    public List<Answer> getAnswers() {
        return this.answers;
    }

    public Boolean checkAnswer(int position){
        if (answers.size() > position){
            return true;
        }else{
            return false;
        }
    }

    public void clearSession(){
        this.answers.clear();
    }
}
