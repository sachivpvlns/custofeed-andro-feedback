package com.custofeed.app.feedback.sync;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.custofeed.app.feedback.R;

public class AuthenticatorActivity extends AccountAuthenticatorActivity implements OnClickListener {

	public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
	public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
	public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
	public final static String PARAM_USER_PASS = "USER_PASS";

	private final String TAG = this.getClass().getSimpleName();

	private AccountManager mAccountManager;
	private String accountType;
	private String authToken;
	private String password;
	private String accountName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.act_login);

		mAccountManager = AccountManager.get(getBaseContext());

		// If this is a first time adding, then this will be null
		accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);
		accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);
		authToken  = getIntent().getStringExtra(ARG_AUTH_TYPE);
		if (accountType == null)
			accountType = getString(R.string.auth_type);
		Log.v(TAG, accountType + ", accountName : " + accountName);
		userSignIn();
		finish();
		//findViewById(R.id.submit).setOnClickListener(this);
	}

	void userSignIn() {

		accountName = getString(R.string.app_name);
		password = getString(R.string.sync_password);

		if (accountName.length() > 0) {
			Bundle data = new Bundle();
			data.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
			data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
			data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
			data.putString(PARAM_USER_PASS, password);

			// Some extra data about the user, demo values
			Bundle userData = new Bundle();
			userData.putString("UserID", "25");
			data.putBundle(AccountManager.KEY_USERDATA, userData);

			// Make it an intent to be passed back to the Android Authenticator
			final Intent res = new Intent();
			res.putExtras(data);

			// Create the new account with Account Name and TYPE
			final Account account = new Account(accountName, accountType);

			// Add the account to the Android System
			if (mAccountManager.addAccountExplicitly(account, password, userData)) {
				// worked
				Toast.makeText(getApplicationContext(), "Account added", Toast.LENGTH_LONG).show();
				mAccountManager.setAuthToken(account, accountType, authToken);
				setAccountAuthenticatorResult(data);
				setResult(RESULT_OK, res);
				finish();
			} else {
				// guess not
				//Toast.makeText(getApplicationContext(), "Account already exists",Toast.LENGTH_LONG).show();
				finish();
			}

		}
	}

	@Override
	public void onClick(View v) {
		userSignIn();
	}

}
