package com.custofeed.app.feedback.network;

/**
 * Created by sachiv on 20-Aug-16.
 */
public class URLS {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public URLS() {
    }
    //Use http://10.0.2.2:port/ for local
    private static final String BASE_URL = "http://www.custofeed.com/";

    public static final String POST_LOGIN = BASE_URL + "rest-auth/login/";
    public static final String POST_REGISTER = BASE_URL + "rest-auth/registration/";
    public static final String POST_RESET_PASSWORD = BASE_URL + "rest-auth/password/reset/";

    public static final long SESSION_EXPIRE_TIME = 24*60*60*1000; //24hrs

    public static final String GET_STORE_DATA = BASE_URL + "companies/api/feedback-forms/";
    public static final String POST_FEEDBACK = BASE_URL + "feedbacks/api/answer/";

}
