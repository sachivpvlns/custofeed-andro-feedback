package com.custofeed.app.feedback.model;

/**
 * Created by sachiv on 4/1/2016. old
 */
public class Answer {
    //private variables
    private int feedback;
    private int question;
    private int option;
    private String text;

    // Empty constructor
    public Answer() {
    }

    // constructor
    public Answer(int feedback, int question, int option, String text) {
        this.feedback = feedback;
        this.question = question;
        this.option = option;
        this.text = text;
    }

    public int getFeedback() {
        return this.feedback;
    }
    public void setFeedback(int feedback) {
        this.feedback = feedback;
    }

    public int getQuestion() {
        return this.question;
    }
    public void setQuestion(int question) {
        this.question = question;
    }

    public int getOption() {
        return this.option;
    }
    public void setOption(int option) {
        this.option = option;
    }

    public String getText() {
        return this.text;
    }
    public void setText(String text) {
        this.text = text;
    }
}
