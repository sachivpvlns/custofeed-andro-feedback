package com.custofeed.app.feedback.utils;

/**
 * Created by sachiv on 23-Aug-16.
 */
public enum SwipeDirection {
    all, left, right, none ;
}
