package com.custofeed.app.feedback.model;

/**
 * Created by sachiv on 4/1/2016. old
 */
public class Question {
    //private variables
    private int id;
    private int pk;
    private String type;
    private String title;

    // Empty constructor
    public Question() {
    }

    // constructor
    public Question(int pk, String type, String title) {
        this.pk = pk;
        this.type = type;
        this.title = title;
    }

    public int getPK() { return this.pk; }
    public void setPK(int pk) {
        this.pk = pk;
    }

    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
