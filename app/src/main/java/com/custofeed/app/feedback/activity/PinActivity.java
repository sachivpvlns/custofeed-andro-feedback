package com.custofeed.app.feedback.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.custofeed.app.feedback.R;
import com.custofeed.app.feedback.model.Answer;
import com.custofeed.app.feedback.model.Customer;
import com.custofeed.app.feedback.model.DatabaseHandler;
import com.custofeed.app.feedback.model.Employee;
import com.custofeed.app.feedback.model.Feedback;
import com.custofeed.app.feedback.model.Option;
import com.custofeed.app.feedback.model.Question;
import com.custofeed.app.feedback.model.Session;
import com.custofeed.app.feedback.network.URLS;
import com.custofeed.app.feedback.sync.AuthenticatorActivity;
import com.custofeed.app.feedback.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PinActivity extends AppCompatActivity {

    private SharedPreferences sharedPref;
    String userEntered;
    final int PIN_LENGTH = 4;
    boolean keyPadLockedFlag = false;
    Context appContext;
    TextView titleView;
    TextView pinBox0;
    TextView pinBox1;
    TextView pinBox2;
    TextView pinBox3;
    TextView[] pinBoxArray;
    TextView statusView;
    TextView button0;
    TextView button1;
    TextView button2;
    TextView button3;
    TextView button4;
    TextView button5;
    TextView button6;
    TextView button7;
    TextView button8;
    TextView button9;
    TextView buttonDelete;
    public String PIN;
    private String POST_URL;
    DatabaseHandler mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        Intent createIntent = new Intent(PinActivity.this, AuthenticatorActivity.class);
        startActivity(createIntent);

        mydb = new DatabaseHandler(this);
        appContext = this;
        userEntered = "";
        buttonDelete = (TextView) findViewById(R.id.buttonDeleteBack);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View v) {

                                                if (keyPadLockedFlag == true) {
                                                    return;
                                                }

                                                if (userEntered.length() > 0) {
                                                    userEntered = userEntered.substring(0, userEntered.length() - 1);
                                                    //pinBoxArray[userEntered.length()].setText("");
                                                    pinBoxArray[userEntered.length()].setBackgroundResource(R.drawable.pin_inactive);
                                                }


                                            }

                                        }
        );

        titleView = (TextView) findViewById(R.id.titleBox);
        pinBox0 = (TextView) findViewById(R.id.pinBox0);
        pinBox1 = (TextView) findViewById(R.id.pinBox1);
        pinBox2 = (TextView) findViewById(R.id.pinBox2);
        pinBox3 = (TextView) findViewById(R.id.pinBox3);

        pinBoxArray = new TextView[PIN_LENGTH];
        pinBoxArray[0] = pinBox0;
        pinBoxArray[1] = pinBox1;
        pinBoxArray[2] = pinBox2;
        pinBoxArray[3] = pinBox3;
        statusView = (TextView) findViewById(R.id.statusMessage);
        View.OnClickListener pinButtonHandler = new View.OnClickListener() {
            public void onClick(View v) {

                if (keyPadLockedFlag == true) {
                    return;
                }

                TextView pressedButton = (TextView) v;


                if (userEntered.length() < PIN_LENGTH) {
                    userEntered = userEntered + pressedButton.getText();

                    //Update pin boxes
                    // pinBoxArray[userEntered.length() - 1].setText("8");
                    pinBoxArray[userEntered.length() - 1].setBackgroundResource(R.drawable.pin_active);

                    if (userEntered.length() == PIN_LENGTH) {

                        if (userEntered.length() == 4) {
                            Boolean flag = false;
                            List<Employee> employees = mydb.getAllEmployees();
                            for (int i = 0; i < employees.size(); i++) {
                                if (employees.get(i).getPin().equals(userEntered)) {
                                    PIN = userEntered;
                                    flag = true;
                                    Session session = (Session) getApplicationContext();
                                    session.clearSession();

                                    sharedPref = getSharedPreferences("data", MODE_PRIVATE);

                                    session.feedback.setPk(Integer.valueOf(sharedPref.getString(SharedPreference.Feedback.PK, null)));
                                    session.feedback.setPin(PIN);
                                    nextActivity();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            pinBoxArray[0].setBackgroundResource(R.drawable.pin_inactive);
                                            pinBoxArray[1].setBackgroundResource(R.drawable.pin_inactive);
                                            pinBoxArray[2].setBackgroundResource(R.drawable.pin_inactive);
                                            pinBoxArray[3].setBackgroundResource(R.drawable.pin_inactive);
                                        }
                                    }, 800);
                                }
                            }
                            if (flag == false) {
                                Toast.makeText(PinActivity.this,
                                        "Invalid PIN", Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        pinBoxArray[0].setBackgroundResource(R.drawable.pin_inactive);
                                        pinBoxArray[1].setBackgroundResource(R.drawable.pin_inactive);
                                        pinBoxArray[2].setBackgroundResource(R.drawable.pin_inactive);
                                        pinBoxArray[3].setBackgroundResource(R.drawable.pin_inactive);
                                    }
                                }, 100);


                            }
                        }
                    }
                } else {
                    //Roll over
                    pinBoxArray[0].setText("");
                    pinBoxArray[1].setText("");
                    pinBoxArray[2].setText("");
                    pinBoxArray[3].setText("");

                    userEntered = "";

                    statusView.setText("");

                    userEntered = userEntered + pressedButton.getText();

                    //Update pin boxes
                    //pinBoxArray[userEntered.length() - 1].setText("8");
                    pinBoxArray[userEntered.length() - 1].setBackgroundResource(R.drawable.pin_active);

                }


            }
        };

        button0 = (TextView) findViewById(R.id.button0);
        button0.setOnClickListener(pinButtonHandler);
        button1 = (TextView) findViewById(R.id.button1);
        button1.setOnClickListener(pinButtonHandler);
        button2 = (TextView) findViewById(R.id.button2);
        button2.setOnClickListener(pinButtonHandler);
        button3 = (TextView) findViewById(R.id.button3);
        button3.setOnClickListener(pinButtonHandler);
        button4 = (TextView) findViewById(R.id.button4);
        button4.setOnClickListener(pinButtonHandler);
        button5 = (TextView) findViewById(R.id.button5);
        button5.setOnClickListener(pinButtonHandler);
        button6 = (TextView) findViewById(R.id.button6);
        button6.setOnClickListener(pinButtonHandler);
        button7 = (TextView) findViewById(R.id.button7);
        button7.setOnClickListener(pinButtonHandler);
        button8 = (TextView) findViewById(R.id.button8);
        button8.setOnClickListener(pinButtonHandler);
        button9 = (TextView) findViewById(R.id.button9);
        button9.setOnClickListener(pinButtonHandler);
        buttonDelete = (TextView) findViewById(R.id.buttonDeleteBack);

        fetchData();
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            postFeedbacks();
        }


        List<Feedback> feedbacks = mydb.getAllFeedbacks();
        for (int i = 0; i < feedbacks.size(); i++) {
            Log.d("FEEDBACK_STATUS", Integer.toString(feedbacks.get(i).getSync()));
        }
    }

    private class LockKeyPadOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            for (int i = 0; i < 2; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            statusView.setText("");

            //Roll over
            pinBoxArray[0].setText("");
            pinBoxArray[1].setText("");
            pinBoxArray[2].setText("");
            pinBoxArray[3].setText("");

            userEntered = "";

            keyPadLockedFlag = false;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    private void fetchData(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLS.GET_STORE_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray json = new JSONArray(response);
                            JSONObject jsonObject = new JSONObject(json.optString(0));
                            JSONObject company = new JSONObject(jsonObject.optString("company"));
                            JSONArray access_stores = new JSONArray(jsonObject.optString("access_store"));
                            JSONObject store = new JSONObject(access_stores.optString(0));
                            JSONArray feedbacks = new JSONArray(store.optString("feedback_forms"));
                            JSONObject feedback = new JSONObject(feedbacks.optString(0));

                            SharedPreferences.Editor prefEditor = sharedPref.edit();
                            prefEditor.putString(SharedPreference.Store.NAME, store.optString("name"));
                            prefEditor.putString(SharedPreference.Company.NAME, company.optString("name"));
                            prefEditor.putString(SharedPreference.Company.SECTOR, company.optString("sector"));
                            prefEditor.putString(SharedPreference.Feedback.TITLE, feedback.optString("title"));
                            prefEditor.putString(SharedPreference.Feedback.PK, feedback.optString("pk"));
                            prefEditor.apply();

                            //Fill PINS DB
                            JSONArray employees = new JSONArray(store.optString("feedback_takers"));
                            mydb.deleteAllEmployees();
                            for (int i=0; i<employees.length(); i++) {
                                Employee employee = new Employee();
                                employee.setPin(employees.optString(i));
                                mydb.addEmployee(employee);
                            }

                            //Fill Questions DB
                            JSONArray questions = new JSONArray(feedback.optString("questions"));
                            mydb.deleteAllQuestions();
                            mydb.deleteAllOptions();
                            for (int i=0; i<questions.length(); i++) {
                                JSONObject qObj = new JSONObject(questions.optString(i));
                                Question question = new Question();

                                question.setPK(qObj.optInt("pk"));
                                question.setType(qObj.optString("type"));
                                question.setTitle(qObj.optString("title"));
                                mydb.addQuestion(question);

                                //Fill Options DB
                                JSONArray options = new JSONArray(qObj.optString("options"));
                                for (int j=0; j<options.length(); j++) {
                                    JSONObject opObj = new JSONObject(options.optString(j));
                                    Option option = new Option();

                                    option.setPK(opObj.optInt("pk"));
                                    option.setTitle(opObj.optString("title"));
                                    option.setQuestion(qObj.optInt("pk"));
                                    mydb.addOption(option);
                                }
                            }
                        } catch (JSONException e) {
                            Toast.makeText(PinActivity.this,
                                    "Invalid Data",Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(PinActivity.this,
                                "Invalid Credentials",Toast.LENGTH_SHORT).show();
                    }
                }){

            @Override
            public Map<String, String> getHeaders() {
                sharedPref = getSharedPreferences("data", MODE_PRIVATE);
                String auth_token = sharedPref.getString(SharedPreference.Account.AUTH_TOKEN, null);
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + auth_token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.ic_launcher)
                    .setTitle("Logout")
                    .setMessage("Are you sure you want to Logout?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean(SharedPreference.Account.IS_LOGGED, false);
                            editor.apply();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
            return true;
        }
//      else if(id == R.id.action_settings){
//            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
//            startActivity(intent);
//            finish();
//        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
    }

    protected void nextActivity(){
        finish();
        Intent myIntent = new Intent(this,FeedbackActivity.class);
        this.startActivity(myIntent);
    }

    private void postFeedbacks(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // add feedback
        List<Feedback> feedbackList = mydb.getUnsyncedFeedbacks();
        Log.d("SYNC_SIZE", Integer.toString(feedbackList.size()));
        for (int i = 0; i < feedbackList.size(); i++) {
            final Feedback feedback = feedbackList.get(i);

            // Customer of the feedback
            final Customer customer = mydb.getCustomer(feedback.getCustomer());
            String name = customer.getName();
            String lastName = " ";
            String firstName= " ";
            if(name.split("\\w+").length>1){

                lastName = name.substring(name.lastIndexOf(" ")+1);
                firstName = name.substring(0, name.lastIndexOf(' '));
            }
            else{
                firstName = name;
            }

            if(lastName.equals("")){
                lastName = " ";
            }

            Log.d("FEEDBACK_ID", Integer.toString(feedback.getID()));

            //Answers of the feedback
            List<Answer> answersList = mydb.getFeedbackAnswers(feedback.getID());

            JSONObject jsonObj = new JSONObject();
            try{
                jsonObj.put("feedback_form", feedback.getPk());

                JSONObject jsonFeedbackTaker = new JSONObject();
                jsonFeedbackTaker.put("pin", feedback.getPin());
                jsonObj.put("feedback_taker", jsonFeedbackTaker);

                jsonObj.put("nps", feedback.getNPS());
                jsonObj.put("comment", feedback.getComment());
                jsonObj.put("timestamp", formatTimestamp(feedback.getCreatedAt()));

                JSONObject jsonFeedbacker = new JSONObject();
                jsonFeedbacker.put("first_name", firstName);
                jsonFeedbacker.put("last_name", lastName);
                jsonFeedbacker.put("mobile", customer.getMobile());
                jsonFeedbacker.put("email", customer.getEmail());

                if(!customer.getDateBDay().equals("")){
                    jsonFeedbacker.put("dob", customer.getDateBDay());
                }
                if(!customer.getDateAnni().equals("")){
                    jsonFeedbacker.put("anni", customer.getDateAnni());
                }
                jsonObj.put("feedbacker", jsonFeedbacker);

                JSONArray answersArray = new JSONArray();

                for(int j=0;j<answersList.size();j++){
                    Answer answer = answersList.get(j);

                    JSONObject jsonAnswer = new JSONObject();
                    jsonAnswer.put("question", answer.getQuestion());
                    if(answer.getOption() > 0){
                        jsonAnswer.put("option",answer.getOption());
                    }
                    jsonAnswer.put("text", answer.getText());

                    answersArray.put(jsonAnswer);
                }
                jsonObj.put("answers", answersArray);

            } catch (JSONException e) {
                Log.d("JSON_ERROR", e.toString());
            }

            JsonObjectRequest req = new JsonObjectRequest(URLS.POST_FEEDBACK, jsonObj,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONObject jsonObject = response;
                            if(jsonObject.has("timestamp")){
                                Feedback fdbk = mydb.getFeedback(feedback.getID());
                                fdbk.setSync(1);
                                mydb.updateFeedback(fdbk);
                                Log.d("RESPONSE", response.toString());
                            }else{
                                Log.d("RESPONSE_ERROR", response.toString());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Volley", error.toString());
                        }
                    }){

                @Override
                public Map<String, String> getHeaders() {
                    String auth_token = sharedPref.getString(SharedPreference.Account.AUTH_TOKEN, null);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Token " + auth_token);
                    return params;
                }
            };
            requestQueue.add(req);
        }
    }

    private String formatTimestamp(String timestamp){
        String[] times = timestamp.split(" ");
        return times[0] + "T" + times[1] + "Z";
    }
}
