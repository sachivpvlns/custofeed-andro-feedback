package com.custofeed.app.feedback.model;

/**
 * Created by sachiv on 4/1/2016. old
 */
public class Employee {
    //private variables
    private String pin;

    // Empty constructor
    public Employee() {
    }

    // constructor
    public Employee(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return this.pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
