package com.custofeed.app.feedback.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.custofeed.app.feedback.model.Answer;
import com.custofeed.app.feedback.model.Customer;
import com.custofeed.app.feedback.model.DatabaseHandler;
import com.custofeed.app.feedback.model.Feedback;
import com.custofeed.app.feedback.network.URLS;
import com.custofeed.app.feedback.utils.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter {
    // Global variables
    // Define a variable to contain a content resolver instance

    public static final String SYNC_FINISHED = "SyncFinished";
    public static final String SYNC_STARTED = "SyncStarted";

    ContentResolver mContentResolver;
    Context context;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        this.context = context;
        mContentResolver = context.getContentResolver();
        Log.i("SyncAdapter earlier", "SyncAdapter");
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /**
     * Perform a sync for this account. SyncAdapter-specific parameters may
     * be specified in extras, which is guaranteed to not be null. Invocations
     * of this method are guaranteed to be serialized.
     *
     */
    private void startScheduleSync() {
        Bundle bundle = new Bundle();
        Account account = new Account(getCurrentAccount(), SharedPreference.PACKAGE_SYNC);
        ContentResolver.addPeriodicSync(account, SharedPreference.PACKAGE_NAME, bundle, 15);
    }

    private String getCurrentAccount() {
        Account[] accounts = AccountManager.get(this.getContext()).getAccountsByType(SharedPreference.PACKAGE_SYNC);
        return accounts.length < 0 ? "" : accounts[0].name;
    }

    private void startForceSync() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        Account account = new Account(getCurrentAccount(), SharedPreference.PACKAGE_SYNC);
        ContentResolver.requestSync(account, SharedPreference.PACKAGE_NAME, bundle);

        ContentResolver.setIsSyncable(account, SharedPreference.PACKAGE_NAME, 1);
        ContentResolver.setSyncAutomatically(account, SharedPreference.PACKAGE_NAME, true);
        startScheduleSync();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Intent in = new Intent(SYNC_STARTED);
        context.sendBroadcast(in);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        final DatabaseHandler db = new DatabaseHandler(this.getContext());

        // add feedback
        List<Feedback> feedbackList = db.getUnsyncedFeedbacks();
        Log.d("SYNC_SIZE", Integer.toString(feedbackList.size()));
        for (int i = 0; i < feedbackList.size(); i++) {
            final Feedback feedback = feedbackList.get(i);

            // Customer of the feedback
            final Customer customer = db.getCustomer(feedback.getCustomer());
            String name = customer.getName();
            String lastName = " ";
            String firstName= " ";
            if(name.split("\\w+").length>1){

                lastName = name.substring(name.lastIndexOf(" ")+1);
                firstName = name.substring(0, name.lastIndexOf(' '));
            }
            else{
                firstName = name;
            }

            if(lastName.equals("")){
                lastName = " ";
            }

            Log.d("FEEDBACK_ID", Integer.toString(feedback.getID()));

            //Answers of the feedback
            List<Answer> answersList = db.getFeedbackAnswers(feedback.getID());

            JSONObject jsonObj = new JSONObject();
            try{
                jsonObj.put("feedback_form", feedback.getPk());

                JSONObject jsonFeedbackTaker = new JSONObject();
                jsonFeedbackTaker.put("pin", feedback.getPin());
                jsonObj.put("feedback_taker", jsonFeedbackTaker);

                jsonObj.put("nps", feedback.getNPS());
                jsonObj.put("comment", feedback.getComment());
                jsonObj.put("timestamp", formatTimestamp(feedback.getCreatedAt()));

                JSONObject jsonFeedbacker = new JSONObject();
                jsonFeedbacker.put("first_name", firstName);
                jsonFeedbacker.put("last_name", lastName);
                jsonFeedbacker.put("mobile", customer.getMobile());
                jsonFeedbacker.put("email", customer.getEmail());

                if(!customer.getDateBDay().equals("")){
                    jsonFeedbacker.put("dob", customer.getDateBDay());
                }
                if(!customer.getDateAnni().equals("")){
                    jsonFeedbacker.put("anni", customer.getDateAnni());
                }
                jsonObj.put("feedbacker", jsonFeedbacker);

                JSONArray answersArray = new JSONArray();

                for(int j=0;j<answersList.size();j++){
                    Answer answer = answersList.get(j);

                    JSONObject jsonAnswer = new JSONObject();
                    jsonAnswer.put("question", answer.getQuestion());
                    if(answer.getOption() > 0){
                        jsonAnswer.put("option",answer.getOption());
                    }
                    jsonAnswer.put("text", answer.getText());

                    answersArray.put(jsonAnswer);
                }
                jsonObj.put("answers", answersArray);

            } catch (JSONException e) {
                Log.d("JSON_ERROR", e.toString());
            }

            JsonObjectRequest req = new JsonObjectRequest(URLS.POST_FEEDBACK, jsonObj,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONObject jsonObject = response;
                            if(jsonObject.has("timestamp")){
                                Feedback fdbk = db.getFeedback(feedback.getID());
                                fdbk.setSync(1);
                                db.updateFeedback(fdbk);
                                Log.d("RESPONSE", response.toString());
                            }else{
                                Log.d("RESPONSE_ERROR", response.toString());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Volley", error.toString());
                        }
                    }){

                @Override
                public Map<String, String> getHeaders() {
                    SharedPreferences sharedPref;
                    sharedPref = getContext().getSharedPreferences("data", Context.MODE_PRIVATE);
                    String auth_token = sharedPref.getString(SharedPreference.Account.AUTH_TOKEN, null);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Token " + auth_token);
                    return params;
                }
            };
            requestQueue.add(req);
        }
        in = new Intent(SYNC_FINISHED);
        context.sendBroadcast(in);
    }

    private String formatTimestamp(String timestamp){
        String[] times = timestamp.split(" ");
        return times[0] + "T" + times[1] + "Z";
    }

}
